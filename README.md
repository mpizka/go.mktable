# About

`mktable` reads lines of deliminated fields from STDIN and prints them as
a nicely formatted table to STDOUT. It is intended for people like me, who
do a lot of plain-text editing.

In the input, rows are seperated by newlines, columns by a deliminator string
(`,` by default, can be set with the `-d` flag). Each row has the same number
of elements.

Built as a stream-editing tool it can easily be used in a CLI context, or
in scripts.


# Install

```
$ go build -o mktable
$ cp mktable ~/.local/bin
```


# Example

```
$ cat exampe.txt
NAME,LATIN NAME,NEURONS
mouse,mus musculus,71000000
honey bee,apis melifera,960000
human,homo sapiens,8.6e+10
roundworm,caenorhabditis elegans,302

$ cat example.txt | mktable -t -s 5
NAME          LATIN NAME                 NEURONS      
----          ----------                 -------      
mouse         mus musculus               71000000     
honey bee     apis melifera              960000       
human         homo sapiens               8.6e+10      
roundworm     caenorhabditis elegans     302
```


# Use in vim

Replace lines 1 through 6 with a 5-spaced, right-aligned table using
these lines as input: 

`:` then `1,6!mktable -s 5 -r`


# Options

`-r` right-align cell content

`-t` underline cells in first row (title)

`-s` spacer width between cells (default 2)

`-d` deliminator between fields (default `,`)

`-h --help` display usage message

`-g` output uses github-flavored-markdown table syntax


# whoami

`mpizka@icloud.com`  
`mpizka@pm.me`
`https://gitlab.com/mpizka`
`https://github.com/mpizka`
