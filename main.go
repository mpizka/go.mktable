package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"
)

func main() {
	title := flag.Bool("t", false, "title line after first row")
	right := flag.Bool("r", false, "right align cell content")
	spacer := flag.Int("s", 2, "spacer width between fields")
	delim := flag.String("d", ",", "set DELIM field deliminator")
	gfm := flag.Bool("g", false, "github-flavor-markdown table syntax")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "USAGE: [options]\noptions:\n")
		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\nRead stdin linewise, build table from DELIM separated fields\n")
		fmt.Fprintf(os.Stderr, "All input lines must have same number of fields\n")
	}
	flag.Parse()

	// -g implies -t
	if *gfm {
		*title = true
	}

	// setup tabwriter
	var twflags uint = 0
	if *right {
		twflags |= tabwriter.AlignRight
	}
	w := tabwriter.NewWriter(os.Stdout, 0, 8, *spacer, ' ', twflags)

	in := bufio.NewScanner(os.Stdin)
	ncols := -1
	for in.Scan() {
		line := strings.Split(in.Text(), *delim)
		if ncols >= 0 && ncols != len(line) {
			fmt.Fprintf(os.Stderr, "table: expected %d fields got %d in %v\n", ncols, len(line), line)
			os.Exit(1)
		}
		// det num cols
		if ncols == -1 {
			ncols = len(line)
		}
		for _, s := range line {
			if *gfm {
				fmt.Fprintf(w, "|%s\t", s)
			} else {
				fmt.Fprintf(w, "%s\t", s)
			}
		}
		fmt.Fprintf(w, "\n")
		if *title {
			doTitle(w, line, *gfm)
			*title = false
		}
	}
	w.Flush()
}

// doTitle writes a deliminating row to the tabwriter
func doTitle(w *tabwriter.Writer, line []string, gfm bool) {
	for _, word := range line {
		if gfm {
			fmt.Fprintf(w, "|%s\t", strings.Repeat("-", len(word)))
		} else {
			fmt.Fprintf(w, "%s\t", strings.Repeat("-", len(word)))
		}
	}
	fmt.Fprintf(w, "\n")
}
